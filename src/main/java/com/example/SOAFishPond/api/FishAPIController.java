/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.SOAFishPond.api;

import com.example.SOAFishPond.Fish;
import com.example.SOAFishPond.FishPool;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lendle
 */
@RestController
public class FishAPIController {
    @GetMapping("/api/fishes")
    public List<Fish> getFishes(){
        return FishPool.fishes;
    }
    
    @PostMapping("/api/fish")
    public void addFish(Fish fish){
        //question1 (20%)
        ///////////////////
    }
    
    @PutMapping("/api/fish")
    public void updateFish(Fish fish){
       //question2 (20%)
       ///////////////////
    }
    
    @DeleteMapping("/api/fish/name/{name}")
    public void removeFish(String name){
        //question3 (20%)
        ///////////////////
    }
}
